<?php
$pos = [4, 1];

$layout = [
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1, 1, 3, 0],
    [0, 1, 0, 0, 0, 1, 1, 0],
    [0, 1, 1, 1, 0, 1, 0, 0],
    [0, 1, 0, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
];

function movePos($move) {
	if (in_array($move,['a','b','c'])) {
		switch ($move) {
			case 'a':
				echo "Moving: UP\n\n";
				break;
			case 'b':
				echo "Moving: RIGHT\n\n";
				break;
			case 'c':
				echo "Moving: DOWN\n\n";
				break;
		}
	} else {
		echo "Error: Invalid move!\n\n";
	}
}

function layoutRenderer($layout, $move = null) {
	global $pos;

	$map = [
		0 => '#',
		1 => '.',
		2 => 'x',
		3 => '$',
	];

	movePos($move);

	if($move=='a'){
		$pos[0] = $pos[0]-1; 
	}elseif($move=='b'){
		$pos[1] = $pos[1]+1; 
	}elseif ($move=='c') {
		$pos[0] = $pos[0]+1; 
	}

	foreach ($layout as $colIndex => $col) {
		foreach($col as $rowIndex => $row) {
			if ($colIndex === $pos[0] && $rowIndex === $pos[1]) {
				echo $map[2];
			} else {
				echo $map[$row];
			}
		}
		echo "\n";
	}
}

function run() {
	global $layout;

	layoutRenderer($layout);

	echo "\nSilahkan input arah [1: UP, 2: RIGHT, 3: DOWN]: ";

	while(true) {
		$move = trim(fgets(STDIN));
		echo "Inputan anda [" . $move . "]\n\n";
		layoutRenderer($layout, $move);
    echo "\nSilahkan input arah [1: UP, 2: RIGHT, 3: DOWN]: ";
		echo "\n";
	}
}

run();